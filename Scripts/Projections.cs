using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Wander
{
    public static class Projections
    {
        public enum Projection
        {
            NotSet,
            WGS84,
            RD,
            WebMercator
        }

        static CoordinateSystem FromEnumProjection( CoordinateSystemFactory f, Projection pr )
        {
            Debug.Assert( pr != Projection.NotSet, "Invalid enum to for a projection." );
            switch (pr)
            {
                case Projection.WGS84:
                    return GeographicCoordinateSystem.WGS84;
                case Projection.RD:
                    return f.CreateFromWkt( "PROJCS[\"Amersfoort / RD New\",GEOGCS[\"Amersfoort\",DATUM[\"Amersfoort\",SPHEROID[\"Bessel 1841\",6377397.155,299.1528128,AUTHORITY[\"EPSG\",\"7004\"]],TOWGS84[565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725],AUTHORITY[\"EPSG\",\"6289\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4289\"]],PROJECTION[\"Oblique_Stereographic\"],PARAMETER[\"latitude_of_origin\",52.15616055555555],PARAMETER[\"central_meridian\",5.38763888888889],PARAMETER[\"scale_factor\",0.9999079],PARAMETER[\"false_easting\",155000],PARAMETER[\"false_northing\",463000],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH],AUTHORITY[\"EPSG\",\"28992\"]]" );
                case Projection.WebMercator:
                    return ProjectedCoordinateSystem.WebMercator;
            }
            return ProjectedCoordinateSystem.WebMercator;
        }

        static CoordinateSystem FromCustomWKT( CoordinateSystemFactory f, string wkt )
        {
            return f.CreateFromWkt( wkt);
        }

        // Usage: ICoordinateTransformation.MathTransform.Transform( ref x, ref y )
        public static ICoordinateTransformation CreateProjection( string fromWkt, Projection to )
        {
            var f = new CoordinateSystemFactory();
            var fromCoordinates = FromCustomWKT( f, fromWkt );
            var toCoordinates = FromEnumProjection( f, to );
            var factory = new CoordinateTransformationFactory();
            return factory.CreateFromCoordinateSystems( fromCoordinates, toCoordinates );
        }

        public static ICoordinateTransformation CreateProjection( Projection from, string toWkt )
        {
            var f = new CoordinateSystemFactory();
            var fromCoordinates = FromEnumProjection( f, from );
            var toCoordinates = FromCustomWKT( f, toWkt );
            var factory = new CoordinateTransformationFactory();
            return factory.CreateFromCoordinateSystems( fromCoordinates, toCoordinates );
        }

        // Usage: ICoordinateTransformation.MathTransform.Transform( ref x, ref y )
        public static ICoordinateTransformation CreateProjection( Projection from, Projection to )
        {
            if (from == Projection.NotSet || to == Projection.NotSet)
                return null;
            var f = new CoordinateSystemFactory();
            var fromCoordinates = FromEnumProjection( f, from );
            var toCoordinates = FromEnumProjection( f, to );
            var factory = new CoordinateTransformationFactory();
            return factory.CreateFromCoordinateSystems( fromCoordinates, toCoordinates );
        }

        // Usage: ICoordinateTransformation.MathTransform.Transform( ref x, ref y )
        // Use this site to lookup WKT's https://epsg.io/4326
        public static ICoordinateTransformation CreateProjection( string fromWKT, string toWKT )
        {
            var f = new CoordinateSystemFactory();
            var fromCoordinates = f.CreateFromWkt( fromWKT );
            var toCoordinates = f.CreateFromWkt( toWKT );
            var factory = new CoordinateTransformationFactory();
            return factory.CreateFromCoordinateSystems( fromCoordinates, toCoordinates );
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static void Transform( ICoordinateTransformation transform, ref Vector3 v )
        {
            var res = transform.MathTransform.Transform( v.x, v.z );
            v.x = (float)res.x;
            v.z = (float)res.y;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static (float x, float y) Transform( ICoordinateTransformation transform, float x, float y )
        {
             return ((float, float))transform.MathTransform.Transform( x, y );
        }
    }
}