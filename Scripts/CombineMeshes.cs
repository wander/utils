using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace Wander
{
    [ExecuteInEditMode()]
    public class CombineMeshes : MonoBehaviour
    {
#if UNITY_EDITOR
        public enum UnusedAction
        {
            Nothing,
            SetInactive,
            Delete
        }

        public UnusedAction unusedAction = UnusedAction.SetInactive;

        internal class Combined
        {
            internal Material mat;
            internal List<Vector3> vertices;
            internal List<Vector3> normals;
            internal List<Vector2> uv;
            internal List<int> indices;
        }

        Dictionary<int, Combined> mergedMeshes;

        public void Combine()
        {
            Undo.RecordObject( gameObject, "Combine Meshes" );

            mergedMeshes = new Dictionary<int, Combined>();
            MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
            List<MeshFilter> usedFilters = new List<MeshFilter>();

            foreach (var mf in meshFilters)
            {
                var mr = mf.GetComponent<MeshRenderer>();
                if (mr == null || mr.tag == "SkipCombine" || mr.sharedMaterial == null || mr.sharedMaterials.Length != mf.sharedMesh.subMeshCount)
                {
                    Debug.LogWarning( "Skipped to combine mesh:" + mf.name + "submesh count and material count do not match or skipped because tag was:NoCombine" );
                    continue;
                }
                usedFilters.Add( mf );

                var baseVertices = mf.sharedMesh.vertices;
                var baseNormals = mf.sharedMesh.normals;
                var baseUvs = mf.sharedMesh.uv;

                for (int j = 0;j < mr.sharedMaterials.Length;j++)
                {
                    var m = mr.sharedMaterials[j];
                    var crc = m.ComputeCRC();
                    if (!mergedMeshes.TryGetValue( crc, out Combined comb ))
                    {
                        comb = new Combined();
                        comb.mat = m;
                        comb.vertices = new List<Vector3>();
                        comb.normals = new List<Vector3>();
                        comb.uv = new List<Vector2>( 0 );
                        comb.indices = new List<int>();
                        mergedMeshes.Add( crc, comb );
                    }
                    var submesh = mf.sharedMesh.GetSubMesh( j );
                    var baseIndices = mf.sharedMesh.GetIndices(j);
                    var tr = mf.transform.localToWorldMatrix;
                    for (int q = 0; q < baseIndices.Length; q++)
                    {
                        var idx = baseIndices[q];
                        var v = baseVertices[idx];
                        comb.vertices.Add( tr.MultiplyPoint( v ) );
                        if (baseNormals != null && baseNormals.Length != 0)
                        {
                            var n = baseNormals[idx];
                            comb.normals.Add( tr.MultiplyVector( n ) );
                        }
                        if (baseUvs != null && baseUvs.Length != 0)
                        {
                            var uv = baseUvs[idx];
                            comb.uv.Add( uv );
                        }
                        else comb.uv.Add( Vector2.zero );

                        comb.indices.Add( comb.vertices.Count-1 );
                    }
                }
            }

            int k =0;
            foreach (var kvp in mergedMeshes)
            {
                GameObject go = new GameObject("Combined_" + (++k));
                var mff  = go.AddComponent<MeshFilter>();
                var mrr  = go.AddComponent<MeshRenderer>();
                Mesh m   = new Mesh();
                m.name = "Combined_"+(k);
                m.indexFormat = IndexFormat.UInt32;
                var comb = kvp.Value;
                m.SetVertices( comb.vertices );
                if (comb.normals != null && comb.normals.Count != 0)
                    m.SetNormals( comb.normals );
                if (comb.uv != null && comb.uv.Count != 0)
                    m.SetUVs( 0, comb.uv );
                m.SetTriangles( comb.indices, 0 );
                m.RecalculateBounds();
                // m.OptimizeReorderVertexBuffer();
                // m.OptimizeIndexBuffers();
                m.Optimize();
                mff.sharedMesh = m;
                mrr.sharedMaterial = comb.mat;
                go.transform.SetParent( gameObject.transform, true );
            }

            if (unusedAction != UnusedAction.Nothing)
            {
                foreach (var mf3 in usedFilters)
                {
                    switch (unusedAction)
                    {
                        case UnusedAction.SetInactive:
                            mf3.gameObject.SetActive( false );
                            break;

                        case UnusedAction.Delete:
                            mf3.gameObject.Destroy();
                            break;
                    }
                }

                if ( unusedAction == UnusedAction.Delete )
                {
                    Cleanup();
                }
            }

            mergedMeshes.Clear();
        }

        void Cleanup() // Not working properly
        {
            while (true)
            {
                int numDestroyed = 0;
                List<Transform> stack = new List<Transform>();
                stack.Add( gameObject.transform );
                while (stack.Count > 0)
                {
                    Transform t = stack[0];
                    stack.RemoveAt( 0 );
                    if (t.childCount > 0)
                    {
                        for (int i = 0;i < t.childCount;i++)
                            stack.Add( t.GetChild( i ) );
                    }
                    else
                    {
                        if (t.GetComponents( typeof( Component ) ).Length <= 1)
                        {
                            t.Destroy();
                            numDestroyed++;
                        }
                    }
                }
                if (numDestroyed <= 0)
                    break;
            }
        }

        public void Combine2()
        {
            Undo.RecordObject( gameObject, "Combine Meshes" );


            MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
            CombineInstance[] combine = new CombineInstance[meshFilters.Length];

            int i = 0;
            while (i < meshFilters.Length)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                combine[i].transform = meshFilters[i].transform.localToWorldMatrix;

           //     if (setOldInactive)
                {
                    meshFilters[i].gameObject.SetActive( false );
                }

                i++;
            }

            transform.GetComponent<MeshFilter>().mesh = new Mesh();
         //   transform.GetComponent<MeshFilter>().mesh.CombineMeshes( combine, mergeSubmeshes );
            transform.gameObject.SetActive( true );
        }

        [CustomEditor( typeof( CombineMeshes ) )]
        [InitializeOnLoad]
        public class CombineMeshesEditor : Editor
        {
            static CombineMeshesEditor()
            {

            }

            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();

                CombineMeshes c = (CombineMeshes)target;
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button( "Combine" ))
                    {
                        c.Combine();
                    }
                }

                GUILayout.EndHorizontal();
            }
        }
#endif
    }
}