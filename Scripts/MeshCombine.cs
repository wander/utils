﻿
using System.Collections.Generic;
using UnityEngine;

namespace Wander
{
    public class MeshCombine
    {
        public class Combined
        {
            internal Material mat;
            internal List<Vector3> vertices;
            internal List<Vector3> normals;
            internal List<Vector2> uv;
            internal List<int> indices;
        }

        public class Input
        {
            public List<int>[] indices;
            public List<Vector3> vertices;
            public List<Vector3> normals;
            public List<Vector2> uvs;
            public Material[] materials;
            public Matrix4x4 localToWorld;
        }

        public Dictionary<int, Combined> Combine( List<Input> inputs )
        {
            Dictionary<int, Combined> mergedMeshes = new Dictionary<int, Combined>();

            for (int i = 0;i < inputs.Count;i++)
            {
                var inp = inputs[i];
                Debug.Assert( inp.materials.Length == inp.indices.Length );
                for (int j = 0;j < inp.materials.Length;j++)
                {
                    var m   = inp.materials[j];
                    var crc = m.ComputeCRC();
                    if (!mergedMeshes.TryGetValue( crc, out Combined comb ))
                    {
                        comb = new Combined();
                        comb.mat = m;
                        comb.vertices = new List<Vector3>();
                        comb.normals = new List<Vector3>();
                        comb.uv = new List<Vector2>( 0 );
                        comb.indices = new List<int>();
                        mergedMeshes.Add( crc, comb );
                    }
                    var tr = inp.localToWorld;
                    for (int k = 0;k < inp.indices[j].Count;k++)
                    {
                        comb.indices.Add( inp.indices[j][k] + comb.vertices.Count );
                    }
                    for (int k = 0; k < inp.vertices.Count; k++)
                    {
                        comb.vertices.Add( tr.MultiplyPoint3x4( inp.vertices[k] ) );
                        if (inp.normals != null && inp.normals.Count != 0)
                        {
                            comb.normals.Add( tr.MultiplyVector( inp.normals[k] ) );
                        }
                        if (inp.uvs != null && inp.uvs.Count != 0)
                        {
                            comb.uv.Add( tr.MultiplyVector( inp.uvs[k] ) );
                        }
                    }
                }
            }

            return mergedMeshes;
        }
    }
}