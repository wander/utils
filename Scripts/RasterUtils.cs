﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Wander
{
    public static class RasterUtils
    {
        /* Usage:
         * Tries to find a valid sample around current (invalid) sample. 
         * E.g.: FilterData( array, 128, 128, 255, 0, 5 );
         * */
        public static void FilterFloatData( this byte[] data, int width, int height, byte invalidSample, byte notFound = 0, int numRings = 5 )
        {
            for (int y = 0;y<height;y++)
            {
                for (int x = 0;x<width;x++)
                {
                    int addr = y*width+x;
                    byte h = data[addr];
                    if ( h==invalidSample )
                    {
                        bool found=false;
                        for (int c = 1;c < numRings && !found;c++)
                        {
                            for (int y2 = y-c;y2 <= y+c &&!found;y2++)
                            {
                                if (GetData(data, x-c, y2, width, height, out byte s ))
                                    if (SetData( data, s, addr, invalidSample )) { found=true; break; }
                            }
                            for (int y2 = y-c;y2 <= y+c &&!found;y2++)
                            {
                                if (GetData(data, x+c, y2, width, height, out byte s))
                                    if (SetData( data, s, addr, invalidSample )) { found=true; break; }
                            }
                            for (int x2 = x-c+1;x2 <= x+c-1 &&!found;x2++)
                            {
                                if (GetData(data, x2, y-c, width, height, out byte s))
                                    if (SetData( data, s, addr, invalidSample )) { found=true; break; }
                            }
                            for (int x2 = x-c+1;x2 <= x+c-1 &&!found;x2++)
                            {
                                if (GetData(data, x2, y+c, width, height, out byte s))
                                    if (SetData( data, s, addr, invalidSample )) { found=true; break; }
                            }
                        }
                        if (!found) // If not done, no neighbour pixel was found.
                        {
                            data[addr] = notFound;
                        }
                    }
                }
            }
        }

        static bool GetData( byte[] data, int x, int y, int width, int height, out byte s )
        {
            s = 0;
            if (x < 0 || x >= width || y < 0 || y >= height)
                return false;
            s = data[y*width+x];
            return true;
        }

        static bool SetData( byte[] data, byte s, int addr, byte invalidSample )
        {
            if (s != invalidSample )
            {
                data[addr] = s;
                return true;
            }
            return false;
        }
    }
}