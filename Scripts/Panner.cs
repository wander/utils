using UnityEngine;

namespace Wander
{
    public class Panner : MonoBehaviour
    {
        public int panButtonIdx     = 0;
        public int rotateButtonIdx  = 1;
        public float panSpeed       = -100;
        public float rotateSpeed    = 1000;
        public float zoomSpeed      = 5000;
        public float heightInfluence = 300;
        public float rotateSmooth   = 5;
        public float panSmooth      = 5;
        public float zoomSmooth     = 5;
        public Bounds bounds = new Bounds(Vector3.zero, Vector3.one * 500);
        public bool allowRotate = true;
        public bool allowZoom = true;
        public bool allowPan = true;
        public bool useBounds = false;

        // Private
        Vector3 panVelocity;
        Vector2 rotateVelocity;
        float zoomVelocity;
        int framesPassed = 0;
        Vector3 originalPosition;
        Quaternion originalRotation;
        float moveModifier = 1;
        float externalSpeedModifier = 0.5f; // like mouse speed modifier
        float externalRotateModifier = 0.5f;

        // This yields only a valid value if there is a Panner used for mouse interaction.
        static bool onGUI;
        public static bool IsCursorOnGUI { get { return onGUI; } }

        private void OnEnable()
        {
            framesPassed = 0;
            onGUI = false;
            originalPosition = transform.position;
            originalRotation = transform.rotation;
        }

        public void ResetOrientation()
        {
            transform.position = originalPosition;
            transform.rotation = originalRotation;
        }

        void Update()
        {
            if ( framesPassed++ < 2 ) return; // to avoid buffered input
            bool pressL = Input.GetMouseButton( panButtonIdx );
            bool pressR = Input.GetMouseButton( rotateButtonIdx );
            if ( Input.GetKeyDown(KeyCode.R))
                ResetOrientation();

            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
            {
                if (Input.GetMouseButtonDown( 0 )) externalSpeedModifier *= 2;
                if (Input.GetMouseButtonDown( 1 )) externalSpeedModifier /= 2;
            }

            // Handle mouse over UI
            if ( UnityEngine.EventSystems.EventSystem.current != null )
            {
                if ( UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() )
                {
                    onGUI = true;
                }
             //   else if ( onGUI && pressL )
             //       return;
                else onGUI = false;
            }

            float groundHeight = 2;
            if ( Physics.Raycast( transform.position, Vector3.down, out RaycastHit hit ) )
            {
                moveModifier = hit.distance / heightInfluence;
                moveModifier = Mathf.Clamp( moveModifier, 0.001f, 1 );
                groundHeight = hit.point.y+2;
            }

            UpdatePanning( pressL );
            UpdateRotate( pressR );
            UpdateZooming( groundHeight );

            // clamp to pannable area
            if (useBounds)
            {
                transform.position = bounds.ClosestPoint( transform.position );
            }
            
            //Debug.Log("MoveModifier: " + moveModifier);
        }

        void UpdatePanning( bool press )
        {
            if(!allowPan) return;
            float dt = Time.deltaTime;
            if (dt > 0.1f)
                return;
            Vector3 panAccel = Vector3.zero;
            if ( !onGUI )
            {
                float x  = (press? Input.GetAxis( "Mouse X" ) : 0) - Input.GetAxis("Horizontal");
                float y  = (press? Input.GetAxis( "Mouse Y" ) : 0) - Input.GetAxis("Vertical");
                x = Mathf.Clamp( x, -1, 1 );
                y = Mathf.Clamp( y, -1, 1 );
                panAccel = new Vector3( x, 0, y ) * panSpeed * moveModifier * externalSpeedModifier;
            }
            panVelocity += panAccel* dt;
            panVelocity *=  MathUtil.Friction( panSmooth, dt );
            if ( panVelocity.magnitude > 0.1 )
            {
                var velocityWorld = transform.TransformDirection( panVelocity );
                velocityWorld.y = 0;
                velocityWorld = velocityWorld.normalized * panVelocity.magnitude;
                transform.position += velocityWorld * dt;
            }
        }

        void UpdateRotate( bool press )
        {
            if (!allowRotate) return;
            float dt = Time.deltaTime;
            if (dt > 0.1f)
                return;
            Vector2 rotateAccel = Vector2.zero;
            if ( press && !onGUI )
            {
                float x  = Input.GetAxis( "Mouse X" );
                float y  = Input.GetAxis( "Mouse Y" );
                x = Mathf.Clamp( x, -1, 1 );
                y = Mathf.Clamp( y, -1, 1 );
                rotateAccel.y = y * rotateSpeed * externalRotateModifier;
                rotateAccel.x = x * rotateSpeed * externalRotateModifier;
            }
            rotateVelocity += rotateAccel * dt;
            rotateVelocity *= MathUtil.Friction( rotateSmooth, dt );
            transform.Rotate( Vector3.up, -rotateVelocity.x * dt, Space.World );
            transform.Rotate( Vector3.right, rotateVelocity.y * dt, Space.Self );
        }

        void UpdateZooming( float groundHeight )
        {
            if (!allowZoom) return;
            float dt = Time.deltaTime;
            if (dt > 0.1f)
                return;
            float zoomAccel = 0;
            float scroll    = Input.GetAxis( "Mouse ScrollWheel" );
            if ( scroll != 0 )
            {
                scroll = Mathf.Clamp( scroll, -1, 1 );
                zoomAccel += scroll * zoomSpeed * moveModifier * externalSpeedModifier;
            }
            zoomVelocity += zoomAccel * dt;
            zoomVelocity *= MathUtil.Friction( zoomSmooth, dt );
            transform.Translate( Vector3.forward * zoomVelocity * dt, Space.Self );
            if (transform.position.y < groundHeight)
            {
                transform.position = new Vector3( transform.position.x, groundHeight, transform.position.z );
            }
        }

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.cyan;
        //    Gizmos.DrawWireCube( bounds.center, bounds.size );
        //}

        public void SetExternalSpeedModifier(float externalModifier)
        {
            externalSpeedModifier = externalModifier;
        }

        public void SetExternalRotateModifier( float _externalRotateModifier )
        {
            externalRotateModifier = _externalRotateModifier;
        }
    }
}