﻿using UnityEngine;

namespace Wander
{
    public static class MathUtil
    {
        public static void ClampAngle360( ref float ang )
        {
            const float oneOver = 1.0f / 360;
            if (ang < 0) ang -= Mathf.FloorToInt( ang * oneOver ) * 360;
            if (ang > 360) ang -= Mathf.FloorToInt( ang * oneOver ) * 360;
        }


        public static int ClampAngle360Int( float ang )
        {
            ClampAngle360( ref ang );
            int iAng = Mathf.RoundToInt(ang);
            Debug.Assert( iAng >= 0 && iAng <= 360 );
            return iAng;
        }

        public static float DistSq( this Vector3 a, Vector3 b )
        {
            Vector3 diff = a-b;
            return Vector3.Dot( diff, diff );
        }

        // Function for life
        public static float Friction( float friction, float dt )
        {
            return Mathf.Clamp( Mathf.Exp( -friction * dt ), 0.0f, 1.0f );
        }

        public static float Smooth( float smooth, float dt )
        {
            return (1.0f - Friction( smooth, dt ));
        }

        public static void AnimateWithAccel( ref float speed, float accel, float maxSpeed, float friction, float dt, bool isAccelerating )
        {
            if (isAccelerating)
            {
                speed += accel*dt;
                if (speed > maxSpeed) speed = maxSpeed;
            }
            else
            {
                speed *= Friction( friction, dt );
            }
        }

        public static bool IsSane( this float f )
        {
            return f >= float.MinValue && f <= float.MaxValue;
        }

        public static bool IsSane( this Vector2 v )
        {
            return v.x.IsSane() && v.y.IsSane();
        }

        public static bool IsSane( this Vector3 v )
        {
            return v.x.IsSane() && v.y.IsSane() && v.z.IsSane();
        }

        public static bool IsSane( this double d )
        {
            return d >= double.MinValue && d <= double.MaxValue;
        }

        public static Vector3 Multiply( this Vector3 v, Vector3 u )
        {
            return Vector3.Scale( v, u );
        }

        public static Vector3 Divide( this Vector3 v, Vector3 u )
        {
            return new Vector3( v.x/u.x, v.y/u.y, v.z/u.z );
        }

        /* Input: t from -1 to 1, k from -1 to 1. 
            Source:  https://dhemery.github.io/DHE-Modules/technical/sigmoid/
            Original Author: Dino Dini. */
        public static float Sigmoid(float t, float k)
        {
            t = Mathf.Clamp( t, -1, 1 );
            k = Mathf.Clamp( k, -1, 1 );
            return  (t - k*t) / (k - 2*k*Mathf.Abs(t) + 1);
        }

        // From bard.google.com.
        public static double CalculateBicubicWeight( double x, double y, double pX, double pY )
        {
            // Calculate the distances between the interpolation point and the four nearest points.
            var dx1 = x - pX;
            var dy1 = y - pY;
            var dx2 = pX + 1 - x;
            var dy2 = pY + 1 - y;

            // Calculate the bicubic weight.
            var weight = (1 - dx1) * (1 - dx2) * (1 - dy1) * (1 - dy2) +
            dx1 * (1 - dx2) * (1 - dy1) * dy2 +
            dx2 * (1 - dx1) * (1 - dy1) * dy2 +
            dx1 * dx2 * (1 - dy1) * dy2 +
            (1 - dx1) * (1 - dx2) * dy1 * (1 - dy2) +
            dx1 * (1 - dx2) * dy1 * dy2 +
            dx2 * (1 - dx1) * dy1 * dy2 +
            dx1 * dx2 * dy1 * dy2;

            return weight;
        }

        //public static double BilinearInterpolate( double x, double y, double[,] values )
        //{
        //    // Find the four points closest to the interpolation point.
        //    var x1 = Math.Floor(x);
        //    var y1 = Math.Floor(y);
        //    var x2 = Math.Ceiling(x);
        //    var y2 = Math.Ceiling(y);

        //    // Calculate the weights for each of the four points.
        //    var w1 = (x2 - x) * (y2 - y);
        //    var w2 = (x - x1) * (y2 - y);
        //    var w3 = (x2 - x) * (y - y1);
        //    var w4 = (x - x1) * (y - y1);

        //    // Calculate the interpolated value.
        //    var interpolatedValue = w1 * values[x1, y1] + w2 * values[x2, y1] + w3 * values[x1, y2] + w4 * values[x2, y2];

        //    return interpolatedValue;
        //}
    }
}