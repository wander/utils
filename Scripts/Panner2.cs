using UnityEngine;

namespace Wander
{
    public class Panner2 : MonoBehaviour
    {
        public int panButtonIdx     = 0;
        public int rotateButtonIdx  = 1;
        public float rotateSpeed    = 1000;
        public float zoomSpeed      = 5000;
        public float heightInfluence = 300;
        public float rotateSmooth   = 5;
        public float panSmooth      = 5;
        public float zoomSmooth     = 5;
        public Bounds bounds = new Bounds(Vector3.zero, Vector3.one * 500);
        public bool allowRotate = true;
        public bool allowZoom = true;
        public bool allowPan = true;
        public bool useBounds = false;

        // Private
        bool startedPanning;
        Vector3 panStartPoint;
        Vector3 panTarget;
        Vector2 rotateVelocity;
        Vector3 originalPosition;
        Quaternion originalRotation;
        float zoomVelocity;
        int   framesPassed = 0;
        float heightModifier;
        float externalSpeedModifier = 0.5f; // like mouse speed modifier
        float externalRotateModifier = 0.5f;

        // This yields only a valid value if there is a Panner used for mouse interaction.
        static bool onGUI;
        public static bool IsCursorOnGUI { get { return onGUI; } }

        private void OnEnable()
        {
            framesPassed = 0;
            onGUI = false;
            originalPosition = transform.position;
            originalRotation = transform.rotation;
            panTarget = transform.position;
        }

        public void ResetOrientation()
        {
            transform.position = originalPosition;
            transform.rotation = originalRotation;
        }

        void Update()
        {
            if ( framesPassed++ < 2 ) return; // To avoid buffered input.
            bool pressL = Input.GetMouseButton( panButtonIdx );
            bool pressR = Input.GetMouseButton( rotateButtonIdx );
            if ( Input.GetKeyDown(KeyCode.R))
                ResetOrientation();

            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
            {
                if (Input.GetMouseButtonDown( 0 )) externalSpeedModifier *= 2;
                if (Input.GetMouseButtonDown( 1 )) externalSpeedModifier /= 2;
            }

            // Handle mouse over UI
            if ( UnityEngine.EventSystems.EventSystem.current != null )
            {
                if ( UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() )
                {
                    onGUI = true;
                }
                else onGUI = false;
            }

            float groundHeight = 2;
            if ( Physics.Raycast( transform.position, Vector3.down, out RaycastHit hit ) )
            {
                heightModifier = Mathf.Clamp01( hit.distance / heightInfluence ) + 0.1f;
                groundHeight = hit.point.y+2;
            }

            UpdatePanning( pressL );
            UpdateRotate( pressR );
            UpdateZooming( groundHeight );

            // Clamp to pannable area.
            if (useBounds)
            {
                transform.position = bounds.ClosestPoint( transform.position );
            }
        }

        bool TryFindGroundPointFromMousePos(out Vector3 point)
        {
            point = Vector3.zero;
            var cam = GetComponent<Camera>();
            var mousePos = Input.mousePosition;
            Ray ray = cam.ScreenPointToRay( mousePos );
            if (ray.direction.y < 0.01f)
            {
                float d = ray.origin.y * (1 / ray.direction.y);
                point = ray.origin + ray.direction * d;
                return true;
            }
            return false;
        }

        void UpdatePanning( bool press )
        {
            if(!allowPan) return;
            float dt = Time.deltaTime;
            if (dt > 0.1f)
                return;
            Vector3 panAccel = Vector3.zero;
            if ( !onGUI )
            {
                if (!press) startedPanning = false;
                if (startedPanning)
                {
                    if (TryFindGroundPointFromMousePos( out var groundPoint ))
                    {
                        var deltaMove = groundPoint - panStartPoint;
                        panTarget = transform.position + deltaMove;
                        transform.position += deltaMove; //  (transform.position-panTarget) * MathUtil.Smooth( panSmooth, dt );
                        if (!TryFindGroundPointFromMousePos( out panStartPoint ))
                        {
                            startedPanning = false;
                        }
                    }
                }
                else if (press && TryFindGroundPointFromMousePos( out panStartPoint ))
                {
                    startedPanning = true;
                }
            }
            else
            {
                startedPanning = false;
            }
            //transform.position += (transform.position-panTarget) * MathUtil.Smooth( panSmooth, dt );
            //if (startedPanning && !TryFindGroundPointFromMousePos( out panStartPoint ))
            //{
            //    startedPanning = false;
            //}
            //if ( panVelocity.magnitude > 0.1 )
            //{
            //    var velocityWorld = transform.TransformDirection( panVelocity );
            //    velocityWorld.y = 0;
            //    velocityWorld = velocityWorld.normalized * panVelocity.magnitude;
            //    transform.position += velocityWorld * dt;
            //}
        }

        void UpdateRotate( bool press )
        {
            if (!allowRotate) return;
            float dt = Time.deltaTime;
            if (dt > 0.1f)
                return;
            Vector2 rotateAccel = Vector2.zero;
            if ( press && !onGUI )
            {
                float x  = Input.GetAxis( "Mouse X" );
                float y  = Input.GetAxis( "Mouse Y" );
                x = Mathf.Clamp( x, -1, 1 );
                y = Mathf.Clamp( y, -1, 1 );
                rotateAccel.y = y * rotateSpeed * externalRotateModifier;
                rotateAccel.x = x * rotateSpeed * externalRotateModifier;
            }
            rotateVelocity += rotateAccel * dt;
            rotateVelocity *= MathUtil.Friction( rotateSmooth, dt );
            transform.Rotate( Vector3.up, -rotateVelocity.x * dt, Space.World );
            transform.Rotate( Vector3.right, rotateVelocity.y * dt, Space.Self );
        }

        void UpdateZooming( float groundHeight )
        {
            if (!allowZoom) return;
            float dt = Time.deltaTime;
            if (dt > 0.1f)
                return;
            float zoomAccel = 0;
            float scroll    = Input.GetAxis( "Mouse ScrollWheel" );
            if ( scroll != 0 )
            {
                scroll = Mathf.Clamp( scroll, -1, 1 );
                zoomAccel += scroll * zoomSpeed * externalSpeedModifier * heightModifier;
            }
            zoomVelocity += zoomAccel * dt;
            zoomVelocity *= MathUtil.Friction( zoomSmooth, dt );
            transform.Translate( Vector3.forward * zoomVelocity * dt, Space.Self );
            if (transform.position.y < groundHeight)
            {
                transform.position = new Vector3( transform.position.x, groundHeight, transform.position.z );
            }
        }

        public void SetExternalSpeedModifier(float externalModifier)
        {
            externalSpeedModifier = externalModifier;
        }

        public void SetExternalRotateModifier( float _externalRotateModifier )
        {
            externalRotateModifier = _externalRotateModifier;
        }
    }
}